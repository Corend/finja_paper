#!/usr/bin/env python3

Usage="""
This program computes bootstrap
"""

try :
        import argparse
except ImportError as err :
        print ("Unable to find argparse, sorry : {}. Exiting...".format(err))
        exit(1)
try :
        import pandas as pd
except ImportError as err :
        print ("Unable to find pandas, sorry : {}. Exiting...".format(err))
        exit(1)
        
parser = argparse.ArgumentParser(description=Usage)

parser.add_argument("-b",help="Replicate",required=True)
parser.add_argument("-s",help="All bootstraps enrich",required=True)
parser.add_argument("-o",help="Observed",required=True)
args=parser.parse_args()


Replicate=open(args.b,"r")

Observed=open(args.o,"r")

Bootstrap_enr=[]
Bootstrap_ero=[]
Bootstrap_eq=[]

for rep, obs in zip(Replicate, Observed):
	
	_rep=rep.split("\t")
	_obs=obs.split("\t")
	
	if (int(_obs[3]) > int(_rep[3])):
		Bootstrap_enr.append(1)
	else:
		Bootstrap_enr.append(0)
		
	if (int(_obs[3]) < int(_rep[3])):
		Bootstrap_ero.append(1)
	else:
		Bootstrap_ero.append(0)

	if (int(_obs[3]) == int(_rep[3])):
		Bootstrap_eq.append(1)
	else:
		Bootstrap_eq.append(0)
		
Observed.close()

data = pd.read_csv(args.s,sep="\t")
SumBootEnr = data['Enrich'].tolist()
SumBootEro = data['Erode'].tolist()
SumBootEq = data['Equal'].tolist()

#print(list(SumBootEnr))

### All_bot = [0 0 0, 0 0 0, 0 0 0 ...]
#All_boot = list(map(lambda s: s.strip(), All_boot))
#All_boot = list(map(lambda s: s.split("\t"), All_boot))

SumBootEnr = [x + y for x, y in zip(SumBootEnr,Bootstrap_enr)]
SumBootEro = [x + y for x, y in zip(SumBootEro,Bootstrap_ero)]
SumBootEq = [x + y for x, y in zip(SumBootEq,Bootstrap_eq)]
outframe=pd.DataFrame({'Enrich':SumBootEnr, 'Erode':SumBootEro, 'Equal':SumBootEq})
outframe.to_csv(args.s,sep='\t',encoding='utf-8')

#All_boot = list(map(str, All_boot))

#All=open(args.s,"w")
#All.write("\n".join(All_boot))
#All.close()
