#!/usr/bin/env python3


Usage="""
This program take as input list and a bed file, and extract features in the list.
"""

try :
        import argparse
except ImportError as err :
        print ("Unable to find argparse, sorry : {}. Exiting...".format(err))
        exit(1)

parser = argparse.ArgumentParser(description=Usage)

parser.add_argument("-b",help="Bed file",required=True)
parser.add_argument("-l",help="List",required=True)
parser.add_argument("-f",help="Select where ?",required=True,type=int)
parser.add_argument("-u",help="If in UTR, yes",required=True)
args=parser.parse_args()

### Start :
BedMerged = open(args.b, "r")
FileListFeatures = open(args.l,"r")
BedMergedFilter = open(args.b+".filter","w")

ListFeatures = FileListFeatures.readlines()
ListFeatures = list(map(lambda s: s.strip(), ListFeatures))

for line in BedMerged :
	if args.u =="True":
		copy = "_".join(list(line.split("\t")[args.f-1].rstrip().split("_")[i] for i in [0,1]))
	else :
		copy = line.split("\t")[args.f-1].rstrip()
		
	if copy in ListFeatures :
		
		BedMergedFilter.write(line)

BedMerged.close()
FileListFeatures.close()
BedMergedFilter.close()
