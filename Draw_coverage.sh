# Folders:
src="/home/cdechaud/shares/Evobooster/Corentin/Papier_finja/Redefine_Finja/Bootstrap_finja/RM/src"
data="/home/cdechaud/shares/Evobooster/Corentin/Papier_finja/Redefine_Finja/Bootstrap_finja/RM/data/Finja"
othersrc="/home/cdechaud/shares/Evobooster/Corentin/Papier_finja/src"

# Variables :
bootstrapdo="oui"
length=2597
sizemin=100
startmin=900
#startmin=0
#endmid=2930
endmid=2400

# Inputs
UCSC_mask="$data/Latipes_finja.ucsc"
#UCSC_mask="$data/Finja_Java_annotation_file.ucsc"
MergedBed="$data/Finja_merged.bed"
#MergedBed="$data/Finja_Java_merged.bed"
structure="$data/Finja_structure.bed"
Query="Finja.fa"
UTR="/home/cdechaud/shares/Evobooster/Corentin/PacBio_Medaka/New_bank_2018-10_all_vislink/tuxedo_gonads/Prepare_tern/Latipes_UTR5.exonic.bed"
#UTR="/home/cdechaud/mnt/Javanicus/Gonadalysis/Prepare_tern/Javanicus_UTR5.exonic.bed"

CDS="/home/cdechaud/shares/Evobooster/Corentin/PacBio_Medaka/New_bank_2018-10_all_vislink/tuxedo_gonads/Prepare_tern2/Latipes_Intron1.good.bed"


 
# Filter UCSC
less $UCSC_mask |sed 1d |awk 'OFS="\t"{if($14>0){print $6,$7, $8,$11,$14-1,$15}else{print $6,$7, $8, $11,$16-1,$15}}' |sed 's/chr//g' >$UCSC_mask.fr

#less $UCSC_mask.fr |awk -v em="$endmid" -v si="$sizemin" -v st="$startmin" 'BEGIN{OFS="\t"}{if($6-$5>si && $6>st && $5<em){print}}' >$UCSC_mask.fr2

# Split large and short ? Merge only short ? No Maybe juste don't merge.
less $UCSC_mask.fr |bedtools merge -d 80 -c 5,6,1 -o collapse,collapse,count |awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"NR"\t"$6}' >$MergedBed
#rm $UCSC_mask.fr2
rm $UCSC_mask.fr

# Melt copies
$othersrc/Merge_TE_from_rm.py -b $MergedBed -l $length

# Apply filter for start and end:
cat $MergedBed.melted |awk -v em="$endmid" -v si="$sizemin" -v st="$startmin" 'BEGIN{OFS="\t"}{if($3-$2>si && $3>st && $2<em){print $1}}' > $MergedBed.select
$othersrc/select_rows.py -b $MergedBed.melted -l $MergedBed.select -f 1 -u "False"

# Get genome coordiantes of kept copies:
cat $MergedBed.melted.filter |awk 'BEGIN{OFS="\t"}{print $4, $5, $6, $1}' |sort |uniq | sort -k1,1 -k2,2n > $MergedBed.genomic.bed

# Merge by TE coordinate.
cat $MergedBed.melted.filter|sort -k1,1 -k2,2n |bedtools merge|awk -v Quer=$Query '{print Quer"\t"$2"\t"$3"\t"$1}' >$MergedBed.melted.merged

# Coverage base for bedtools coverage
$othersrc/Create_coverage_base.py -s 1 -w $length -c $Query -f $data/Background_$Query.bed

# Coverage on TE consensus
bedtools coverage -a $data/Background_$Query.bed -b $MergedBed.melted.merged > $data/Coverage_$Query.bed

# Copy number
ncop=`less $MergedBed.select |sort |uniq |wc -l`

# In UTR :
cat $MergedBed.melted.filter |awk 'BEGIN{OFS="\t"}{print $4, $5, $6, $1, $2, $3}' > $MergedBed.forintersect.bed
intersectBed -a $MergedBed.forintersect.bed -b $UTR -wa -wb >$MergedBed.UTR.intersect
cat $MergedBed.UTR.intersect |cut -f4 |sort |uniq > $MergedBed.copiesUTR
cat $MergedBed.UTR.intersect |cut -f10 |sort |uniq |cut -f2 -d "-" |sort |uniq >$data/Genes.Finja.UTR
$othersrc/select_rows.py -b $MergedBed.melted.merged -l $MergedBed.copiesUTR -f 4 -u "False"
$othersrc/Create_coverage_base.py -s 1 -w $length -c $Query -f $data/Background_$Query.UTR.bed
bedtools coverage -a $data/Background_$Query.UTR.bed -b $MergedBed.melted.merged.filter > $data/Coverage_$Query.UTR.bed
ncoputr=`less $MergedBed.melted.merged.filter |cut -f4 |sort |uniq |wc -l`
mv $MergedBed.melted.merged.filter $MergedBed.melted.merged.filter.UTR.$sizemin

# In CDS :
#intersectBed -a $MergedBed -b $CDS -wa -wb >$MergedBed.CDS.intersect
#less $MergedBed.CDS.intersect |cut -f6 |sort |uniq |awk '{print "Copy_"$0}' > $MergedBed.copiesCDS
#less $MergedBed.CDS.intersect |cut -f10 |sort |uniq |cut -f2 -d "-" |sort |uniq >$data/Genes.Finja.CDS
#$othersrc/select_rows.py -b $MergedBed.melted.merged -l $MergedBed.copiesCDS -f 4 -u "False"

# If Random :
#less $MergedBed.select |sort |uniq |shuf -n $ncoputr >$data/RandTest
#$othersrc/select_rows.py -b $MergedBed.melted.merged -l $data/RandTest -f 4 -u "False"
#$othersrc/Create_coverage_base.py -s 1 -w $length -c $Query -f $data/Background_$Query.CDS.bed
#bedtools coverage -a $data/Background_$Query.CDS.bed -b $MergedBed.melted.merged.filter > $data/Coverage_$Query.CDS.bed
#ncopcds=`less $MergedBed.melted.merged.filter |cut -f4 |sort |uniq |wc -l`

# In CDS :
#cat $MergedBed.melted.filter |awk 'BEGIN{OFS="\t"}{print $4, $5, $6, $1, $2, $3}' > $MergedBed.forintersect.bed
intersectBed -a $MergedBed.forintersect.bed -b $CDS -wa -wb >$MergedBed.CDS.intersect
cat $MergedBed.CDS.intersect |cut -f4 |sort |uniq > $MergedBed.copiesCDS
cat $MergedBed.CDS.intersect |cut -f10 |sort |uniq |cut -f2 -d "-" |sort |uniq >$data/Genes.Finja.CDS
$othersrc/select_rows.py -b $MergedBed.melted.merged -l $MergedBed.copiesCDS -f 4 -u "False"
$othersrc/Create_coverage_base.py -s 1 -w $length -c $Query -f $data/Background_$Query.CDS.bed
bedtools coverage -a $data/Background_$Query.CDS.bed -b $MergedBed.melted.merged.filter > $data/Coverage_$Query.CDS.bed
ncopcds=`less $MergedBed.melted.merged.filter |cut -f4 |sort |uniq |wc -l`
mv $MergedBed.melted.merged.filter $MergedBed.melted.merged.filter.CDS.$sizemin

# Run R :
Rscript $othersrc/Draw_profile.R $data/Coverage_$Query.bed $ncop $structure $data/Coverage_$Query.UTR.bed $ncoputr $data/Coverage_$Query.CDS.bed $ncopcds

# Bootstrap :
#$src/Bootstrap.sh
if [ $bootstrapdo == "oui" ]; then
i="0"
boot=1000
j="0"
printf "Enrich\tErode\tEqual\n"> $data/$Query.boot.all.csv
while [ $j -lt $length ]; do
        printf "0\t0\t0\n" >> $data/$Query.boot.all.csv
        j=$[$j+1]

done

while [ $i -lt $boot ]
do
        # Select random TEs :
        less $MergedBed.select |sort |uniq |shuf -n $ncoputr >$data/RandTest

        # Extract rows :
        $othersrc/select_rows.py -b $MergedBed.melted.merged -l $data/RandTest -f 4 -u "False"

        # Create coverage file for the random TE selected
        $othersrc/Create_coverage_base.py -s 1 -w $length -c $Query -f $data/Background_$Query.rand.bed
        bedtools coverage -a $data/Background_$Query.rand.bed -b $MergedBed.melted.merged.filter > $data/Coverage_$Query.rand.bed

        # Add to bootstrap      
        $othersrc/compare_bootstraps.py -b $data/Coverage_$Query.rand.bed -s $data/$Query.boot.all.csv -o $data/Coverage_$Query.UTR.bed

        #echo "lo"       
        i=$[$i+1]
done
fi
