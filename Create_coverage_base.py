#!/usr/bin/env python3

### Biopython to get ORFS.
from Bio import SeqIO

Usage="""This program creates a base bed for bed coverage  
"""

try :
        import argparse
except ImportError as err :
        print ("Unable to find argparse, sorry : {}. Exiting...".format(err))
        exit(1)

parser = argparse.ArgumentParser(description=Usage)

parser.add_argument("-s",help="Window size",required=True,type=int)
parser.add_argument("-w",help="Region length",required=True,type=int)
parser.add_argument("-c",help="Chromosome name",required=True)
parser.add_argument("-f",help="Output file name",required=True)
args=parser.parse_args()



### Start :
BedOut = open(args.f, "w")

i=0
while i < args.w :
	BedOut.write(str(args.c)+"\t"+str(i)+"\t"+str(i+args.s)+"\n")
	i+=args.s

BedOut.close()


#def file_to_list(lst_file):
	#loaded = open(lst_file,"r")
	#list_output = []
	#for line in loaded:
		#list_output.append(line)
	#loaded.close()
	#return(list_output)
	

#bed_input = open(args.b,"r")
#bed_output = open(args.b+".sorted","w")

#list_keep = file_to_list(args.g)

#for line in bed_input:
	#bed_fields = line.split("\t")
	##print(bed_fields)
	#if bed_fields[int(args.c)] in list_keep:
		#bed_output.write("\t".join(bed_fields))


#bed_output.close()
#bed_input.close()
	
