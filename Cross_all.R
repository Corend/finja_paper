#!/home/cdechaud/softwares/R-3.5.1/bin/Rscript

args = commandArgs(trailingOnly=FALSE)

## Parse the arguments
args = commandArgs(trailingOnly=TRUE)
# args = c("All.UTR5.csv",
#         "/home/cdechaud/shares/Evobooster/Corentin/PacBio_Medaka/New_bank_2018-10_all_vislink/tuxedo_gonads/Count_tables/Latipes_Expression_table.csv",
#         "/home/cdechaud/shares/Evobooster/Corentin/PacBio_Medaka/New_bank_2018-10_all_vislink/tuxedo_gonads/Count_tables/Latipes_Expression_transcript_table.csv",
#         "UTR5",
#         "Finja",
#         "TE.list",
#         "/home/cdechaud/shares/Evobooster/Corentin/Papier_finja/Cross_all/data2")
TransExp = args[3]
GeneExp = args[2]
Corresp = args[1]
Loc = args[4]
TEfam = args[5]
TElist=args[6]
outdir=paste(args[7],"/",sep="")

PropGenes <- c(3293/17254,3600/17254,10361/17254)
PropTranscr <- c(4162/30505,5051/30505,21292/30505)
# Start :
library(dplyr,quietly = T,warn.conflicts = F)
library(ggplot2,quietly = T,warn.conflicts = F)
library(ggtern,quietly = T,warn.conflicts = F)
library(reshape2,quietly = T,warn.conflicts = F)
# Read files :
TransExp <- read.table(TransExp,h=T,sep = "\t")
GeneExp <- read.table(GeneExp,h=T,sep = "\t")
Corresp <- read.table(Corresp,h=F,sep = "\t",col.names = c("transcript","gene","te"))
TElist <- read.table(TElist)$V1
TElist <- data.frame(te=as.character(TElist),count=as.numeric(0))

# Gene number
manual=FALSE
if (manual){
  GeneList <- unique(Corresp$gene)
  TransList <- unique(Corresp$transcript)
  
  GeneNum <- length(GeneList)
  TransNum <- length(TransList)
  
  GeneDE <- table(GeneExp[GeneExp$Gene%in%GeneList,"Organ"])
  TransDE <- table(TransExp[TransExp$Gene%in%TransList,"Organ"])
  
  print(paste("Gene number with",TEfam,"in",Loc,":",GeneNum))
  print(GeneDE)
  print(paste("Transcript number with",TEfam,"in",Loc,":",TransNum))
  print(TransDE)
}

mychisqGene <- function(vec){
    return(chisq.test(vec,p=PropGenes)$p.value)
}
mychisqTr <- function(vec){
    return(chisq.test(vec,p=PropTranscr)$p.value)
}
# Gene number all
for (i in c("gene","transcript")){
  print(i)
  TotalGeneNum <- Corresp %>%
    group_by(te) %>%
    summarise(count=length(unique(eval(parse(text=i)))))
  
  TotalGeneNum <- rbind(TotalGeneNum,TElist)
  TotalGeneNum <- TotalGeneNum %>%
    group_by(te) %>%
    summarise(count=sum(count))
  
  geneh <- ggplot(data = TotalGeneNum,mapping = aes(x=count))+
     geom_histogram(alpha=0.5,fill="#3d45b7",color="#3d45b7",binwidth=1)+
     theme_bw()+xlab(paste("Number of genes with the TE in their",Loc))+ylab("Number of TE family")
  # 
  
  ggsave(file=paste(outdir,i,"_hist_",TEfam,"_",Loc,".svg",sep=""), plot=geneh, width=10, height=8)
  write.table(x = TotalGeneNum[order(TotalGeneNum$count,decreasing = T),],file = paste(outdir,i,"_count_",Loc,"_",TEfam,".csv",sep=""),append = F,quote = F,sep = "\t",row.names = F)
  # 
  # Ternary with gene associated :
  Exp = ifelse(test = (i=="gene"),yes = "GeneExp",no = "TransExp")
  Corresp_exp = unique(merge(x = Corresp,y = eval(parse(text=Exp)),by.x = i,by.y="Gene")[,c(i,"te","Organ")])
  AllTE = unique(Corresp_exp[,c(i,"Organ")])
  AllTE = table(AllTE$Organ)
  Corresp_exp <- Corresp_exp %>%
    group_by(.dots=c("te","Organ")) %>%
    summarise(count=length(Organ))
  
  Corresp_exp <- dcast(Corresp_exp,formula = te~Organ)
  Corresp_exp[is.na(Corresp_exp)] <- 0
  AllSum <- apply(X = Corresp_exp[,-1],MARGIN = 2,FUN = sum)
  AllPerc <- AllSum/sum(AllSum)
  Corresp_exp <- Corresp_exp[apply(Corresp_exp[,-1],1,sum)>5,]
  Percs <- Corresp_exp[,-1]/apply(Corresp_exp[,-1],1,sum)
  colnames(Corresp_exp) <- c("te","F","M","NotDE")
  
  TotGenes = apply(X = Corresp_exp[,-1],MARGIN = 1,FUN = sum)  
  if (i=="gene"){
    pvals=apply(X = Corresp_exp[,-1],MARGIN = 1,FUN = mychisqGene)
  }else if (i =="transcript"){
    pvals=apply(X = Corresp_exp[,-1],MARGIN = 1,FUN = mychisqTr)
  }
  
  pbonf=length(pvals)*pvals
  pbonf=ifelse(pbonf>1,1,pbonf)
  PercsB <- cbind(te=Corresp_exp$te,Percs,tot=TotGenes,pval=pvals,pbonf=pbonf)
  PercsB$te <- as.character(PercsB$te)
  PercsB <- rbind(PercsB,c("All",AllPerc,tot=sum(AllSum),0,0))
  PercsB <- rbind(PercsB,c("AllExcl",AllTE/sum(AllTE),tot=sum(AllTE),0,0))
  if (i=="gene"){
    PercsB <- rbind(PercsB,c("Genes",PropGenes,tot=sum(PropGenes),0,0))
  }else if (i=="transcript"){
    PercsB <- rbind(PercsB,c("Transcripts",PropTranscr,tot=sum(PropTranscr),0,0))
  }
  colnames(PercsB) <- c("te","Fem","Male","NotDE","tot","pvals","pbonf")
  
  PercsB <- transform(PercsB,Fem=as.numeric(Fem),Male=as.numeric(Male),NotDE=as.numeric(NotDE),tot=as.numeric(tot),pvals=as.numeric(pvals),pbonf=as.numeric(pbonf))
  rownames(PercsB) <- seq(1:dim(PercsB)[1])
  colfunc <- colorRampPalette(c("orange","brown","black"))
  #write.table(x = sum(pbonf<0.05),file = paste(outdir,i,"_count_of_go_",Loc,".csv",sep=""),append = T,quote = F,sep = "\t",row.names = F,col.names = F)
  maxitri <- ggtern(data = PercsB, aes(x=Male,y= NotDE,z= Fem,alpha=pbonf<0.05,color=-log10(pvals))) +
      geom_point()+theme_bw()+scale_alpha_manual(values=c(0.1,1))+scale_color_gradientn(colours=colfunc(10),limits=c(0,10))+
      theme(panel.grid.major=element_line())+
      geom_text(aes(label=ifelse(pbonf<0.05,as.character(te),'')),hjust=0,vjust=0,size=1.5)

  ggsave(file=paste(outdir,i,"_tern_",TEfam,"_",Loc,".svg",sep=""), plot=maxitri, width=10, height=8)
  write.table(x = PercsB,file = paste(outdir,i,"_data_",TEfam,"_",Loc,".csv",sep=""),append = F,quote = F,sep = "\t",row.names = F,col.names = T) 
  
  
  }



