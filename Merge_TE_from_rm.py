#!/usr/bin/env python3


Usage="""
This program take as input a RM output file for a single TE family and build a consensus coverage.
"""

try :
        import argparse
except ImportError as err :
        print ("Unable to find argparse, sorry : {}. Exiting...".format(err))
        exit(1)

parser = argparse.ArgumentParser(description=Usage)

parser.add_argument("-b",help="Repeat Masker output, ucsc format",required=True)
parser.add_argument("-l",help="Consensus length",required=True,type=int)
args=parser.parse_args()

### Start :
BedMerged = open(args.b, "r")
BedMelt = open(args.b+".melted","w")

### Object row
class row:
	"""
	A class to describe a row of the merged bed, meaning a TE copy.
	"""
	
	def __init__(self,attributes):
		attr=attributes.split("\t")
		self._copy=attr[5].rstrip()
		self._starts=attr[3].split(",")
		self._ends=attr[4].split(",")
		self._mergen=attr[6].rstrip()
		self._chrstart=attr[1]
		self._chrend=attr[2]
		self._chr=attr[0]
	
	def aff(self):
		"""
		Display the copy
		"""
		print(self.get_copy()+"\t"+",".join(self.get_starts())+"\t"+",".join(self.get_ends())+"\t"+self.get_chr())
	
	def get_chrstart(self):
		return self._chrstart
	
	def get_chrend(self):
		return self._chrend	
		
	def get_mergen(self):
		return self._mergen
	
	def get_copy(self):
		return self._copy
	
	def get_starts(self):
		return self._starts
		
	def get_ends(self):
		return self._ends
	
	def get_chr(self):
		return self._chr


for copies in BedMerged: # For each copies
	copy=row(copies)	 # Object containing copy.
	
	HitNum = len(copy.get_starts())
	
	sumlengcop=0
	
	for hit in range(HitNum):
		
		sumlengcop=sumlengcop+(int(copy.get_ends()[hit])-int(copy.get_starts()[hit]))
		
		sizeatm = int(sumlengcop/args.l) # 0 = shorter than cons, 1 more, 2 more than 2 times etc ..
		
		BedMelt.write("Copy_"+copy.get_copy()+"_"+str(sizeatm) + "\t" + copy.get_starts()[hit] + "\t" + copy.get_ends()[hit] + "\t" + copy.get_chr() + "\t" + copy.get_chrstart()+ "\t" + copy.get_chrend() + "\n")
		
	#copy.aff()
	#exit(1)

BedMerged.close()
BedMelt.close()
