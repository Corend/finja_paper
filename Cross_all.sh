#!/bin/bash

# If argument passed are not expected:
usage() { echo "Usage: $0 [-f <TE family>] [-l <localisation>] [-g <gene expression>] [-e <transcript expression>] [-b <bed of localisation>] [-k <bed of TE>] [-d <outdir>]" 1>&2; exit 1; }

# Retrieve the arguments:
while getopts ":f:l:g:e:b:k:d:" o; do
    case "${o}" in
        f)
            TEfam=${OPTARG}
            ;;
	l)
	    Loc=${OPTARG}
	    ;;
	g)
	    GeneExp=${OPTARG}
	    ;;
	e)
	    TransExp=${OPTARG}
	    ;;
	b)
	    BedLoc=${OPTARG}
	    ;;
	k)
	    BedTE=${OPTARG}
	    ;;
	d)
	    outdir=${OPTARG}
	    ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

# If arguments are missing:
if [ -z "${TEfam}" ] || [ -z "${Loc}" ] || [ -z "${GeneExp}" ] || [ -z "$TransExp" ] || [ -z "$BedTE" ] || [ -z "$BedLoc" ]; then	
	usage
fi

# Intersect bed of TE with bed of Loc
bedtools intersect -a $BedLoc -b $BedTE -wa -wb |cut -f4,8 |awk 'BEGIN{OFS="\t";FS="\t"}{split($1,s,"-");print s[1], s[2], $2}' |uniq  >${outdir}/$TEfam.$Loc.csv

# Create list of all TEs: 1229
less $BedTE |cut -f4 |sort |uniq >$outdir/TE.list

./Cross_all.R $outdir/$TEfam.$Loc.csv $GeneExp $TransExp $Loc $TEfam $outdir/TE.list $outdir
